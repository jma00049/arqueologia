﻿using UnityEngine;
using UnityEditor;
using Newtonsoft.Json;
using Newtonsoft.Json.UnityConverters.Math;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class CustomUnityEditor : EditorWindow
{
    [MenuItem("Json/Export active scene")]
    public static void CustomEditorWindow()
    {
        GameObject[] objetos = SceneManager.GetActiveScene().GetRootGameObjects();
        List<Objeto> objetosEscena = new List<Objeto>();
        foreach (GameObject obj in objetos)
        {
            if (!obj.tag.Equals("MainCamera"))
            {
                Objeto o1 = new Objeto();
                o1.nombre = obj.name;
                o1.posicion = obj.transform.position;
                o1.rotacion = obj.transform.rotation;
                o1.escala = obj.transform.localScale;

                objetosEscena.Add(o1);       
            }
            
        }
        string output = JsonConvert.SerializeObject(objetosEscena);
        // serialize JSON to a string and then write string to a file
        string path = SceneManager.GetActiveScene().name + ".json";
        File.WriteAllText(@".\"+path, JsonConvert.SerializeObject(objetosEscena));
        Debug.Log(output);
    }
}