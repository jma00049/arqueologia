﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Objeto
{
    public string nombre { get; set; }
    public Vector3 posicion { get; set; }
    public Quaternion rotacion { get; set; }
    public Vector3 escala { get; set; }

    public Objeto()
    {
        nombre = "";
        posicion = new Vector3();
        rotacion = new Quaternion();
        escala = new Vector3();
    }
}
