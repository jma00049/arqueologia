﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Parabox.CSG;
public class CortadoDeObjetos : MonoBehaviour
{
    public GameObject ciudad_romana_carraque;
    public GameObject pipa_arcilla;

    private GameObject composite;
    // Start is called before the first frame update
    void Start()
    {
        // Perform boolean operation
        CSG_Model result = Boolean.Subtract(ciudad_romana_carraque, pipa_arcilla);

        // Create a gameObject to render the result
        composite = new GameObject();
        composite.name = "result";
        composite.AddComponent<MeshFilter>().sharedMesh = result.mesh;
        composite.AddComponent<MeshRenderer>().sharedMaterials = result.materials.ToArray();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
